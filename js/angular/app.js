var app = angular.module('taller', ['ui.router']);

app.controller('MainController', ['$scope', function($scope) {

    $scope.navbar = 
        [
        {
            name: 'Inicio',
            href: '/#/',
            target: '',
            description: 'Ir a la página principal'
        },
        {
            name: 'Convertir temperatura',
            href: '/#/calc',
            target: '',
            description: ''
        },
        {
            name: 'Contacto',
            href: '/#/contact',
            target: '',
            description: 'Escribanos desde un formulario'
        }
    ]
}])

app.controller('ContactController', ['$scope', function($scope) {
// Here's my data model
var ViewModel = function(name, mobile, email, subject) {
    this.name = ko.observable(name);
    this.mobile = ko.observable(mobile);
    this.email = ko.observable(email);
    this.subject = ko.observable(subject);
 
    this.fullData = ko.computed(function() {
        return "Entonces " + this.name().toUpperCase() + ", en resumen usted escribe a nombre de " + this.email().toUpperCase() + " un mensaje con el asunto " + this.subject().toUpperCase() + " y por varas le deja su número " + this.mobile();
    }, this);
};
 
ko.applyBindings(new ViewModel("","","","")); // This makes Knockout get to work
}])

app.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('home', {
    url: "/",
    templateUrl: "templates/home.html"
  })
  
  .state('calc', {
    url: "/calc",
    templateUrl: "templates/calc.html"
  })

  .state('contact', {
      url: "/contact",
      templateUrl: "templates/contact.html",
      controller: 'ContactController'
    });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/');
});