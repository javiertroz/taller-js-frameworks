function c2f(c) {
  return 9/5 * c + 32;
}

function f2c(f) {
  return 5/9 * (f - 32);
}

var Temperature = Backbone.Model.extend({
	defaults: {
		celsius: 0,
      fahrenheit: 0
	}
});


var TemperatureView = Backbone.View.extend({
  el: document.body,
  model: new Temperature(),
  events: {
    "input": "update"
  },
  initialize: function() {
    this.convert = {
      celsius: {
        f: c2f,
        target: 'fahrenheit'
      },
      fahrenheit: {
        f: f2c,
        target: 'celsius'
      }
    };
    this.render();
  },
  render: function () {
    this.$('#celsius').val(this.model.get('celsius'));
    this.$('#fahrenheit').val(this.model.get('fahrenheit'));
  },
  update: function (event) {
    var id = event.target.id,
        value = event.target.value,
        converted = this.convert[id].f(value),
        target = this.convert[id].target;
    
    this.model.set(id, value);
    this.model.set(target, converted);
    this.$('#' + target).val(converted);
  }
});

var temperatureView = new TemperatureView();
