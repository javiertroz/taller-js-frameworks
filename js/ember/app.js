var App = Ember.Application.create();

App.ApplicationController = Ember.Controller.extend({
  format: "LL",
  date: new Date(),
  formattedDate: function() {
    var date = this.get('date'),
        format = this.get('format');
    return moment(date).format(format);
  }.property('format', 'date')
});

App.ApplicationRoute = Ember.Route.extend({
  setupController: function(controller) {
    controller.set('createdAt', new Date(2011, 8, 2));
  }
});

Ember.Handlebars.registerBoundHelper('format-date', function(format, date) {
  return moment(date).format(format);
});